import datetime

import gc
import os
import random
import abc

import comet_ml
import keras
import numpy as np
import tensorflow as tf
from keras.callbacks import Callback
from keras.utils import multi_gpu_model

import deepprofiler.dataset.utils
import deepprofiler.imaging.cropping
import deepprofiler.learning.validation

from plugins.modules.clr_callback import *

from art.classifiers import KerasClassifier
from art.data_generators import KerasDataGenerator
from art.attacks.evasion.projected_gradient_descent import ProjectedGradientDescent
from art.defences.trainer.adversarial_trainer import AdversarialTrainer

import logging
from tqdm import tqdm


##################################################
# This class should be used as an abstract base
# class for plugin models.
##################################################


class DeepProfilerModel(abc.ABC):

    def __init__(self, config, dset, crop_generator, val_crop_generator):
        self.feature_model = None
        self.parallel_model = None
        self.adversarial_model = None
        self.loss = None
        self.optimizer = None
        self.config = config
        self.dset = dset
        self.train_crop_generator = crop_generator(config, dset)
        self.val_crop_generator = val_crop_generator(config, dset)
        self.random_seed = None
        if "comet_ml" not in config["train"].keys():
            self.config["train"]["comet_ml"]["track"] = False

    def seed(self, seed):
        self.random_seed = seed
        random.seed(seed)
        np.random.seed(seed)
        tf.set_random_seed(seed)

    def train(self, epoch=1, metrics=["accuracy"], verbose=1):
        adversary = self.config["train"]["adversarial"]
        multi_gpu = self.config["train"]["multi_gpu"]
        pre_trained = self.config["train"]["pretrained"]
        # Raise ValueError if feature model isn't properly defined
        check_feature_model(self)
        # Print model summary
        if verbose != 0:
            self.feature_model.summary()
        # Make model multi-gpu
        if multi_gpu:
            gpu_count = len(self.config["train"]["gpus"].split(","))
            self.parallel_model = multi_gpu_model(self.feature_model, gpus=gpu_count)
        # Compile model
        if not multi_gpu:
            self.feature_model.compile(self.optimizer, self.loss, metrics)
        else:
            self.parallel_model.compile(self.optimizer, self.loss, metrics)
        # Load pre trained weights
        if pre_trained:
            load_pretrained_weights(self)
        # Create classifier wrapper
        if adversary:
            self.adversarial_model = KerasClassifier(model=self.feature_model)
        # Create comet ml experiment
        experiment = setup_comet_ml(self)
        # Create tf configuration
        configuration = tf_configure(self)
        # Start train crop generator
        crop_session = start_crop_session(self, configuration)
        # Start val crop generator
        val_session, x_validation, y_validation = start_val_session(self, configuration)
        # Create main session
        main_session = start_main_session(configuration)
        # Add Extra Callbacks
        extra_callbacks = []
        if self.config["train"].get("scheduled_lr", None) == 'clr':
            extra_callbacks.append(setup_clr_callback(self))
        # Load weights
        if verbose != 0:  # verbose is only 0 when optimizing hyperparameters
            if adversary:
                adversarial_load_weights(self, epoch)
            else:
                load_weights(self, epoch)
        # Create callbacks
        callbacks = setup_callbacks(self)  # adversarial callbacks are generated on each epoch
        # Create params (epochs, steps, log model params to comet ml)
        epochs, steps = setup_params(self, experiment)
        keras.backend.get_session().run(tf.initialize_all_variables())
        # Train model
        if not adversary:
            if not multi_gpu:
                self.feature_model.fit_generator(
                    generator=self.train_crop_generator.generate(crop_session),
                    steps_per_epoch=steps,
                    epochs=epochs,
                    callbacks=callbacks + extra_callbacks,
                    verbose=verbose,
                    initial_epoch=epoch - 1,
                    validation_data=(x_validation, y_validation)
                )
            else:
                self.parallel_model.fit_generator(
                    generator=self.train_crop_generator.generate(crop_session),
                    steps_per_epoch=steps,
                    epochs=epochs,
                    callbacks=callbacks + extra_callbacks,
                    verbose=verbose,
                    initial_epoch=epoch - 1,
                    validation_data=(x_validation, y_validation)
                )
        else:
            # Make adversarial attack
            attacks = ProjectedGradientDescent(self.adversarial_model,
                                               eps=self.config["train"]["adversary"]["epsilon"],
                                               eps_step=self.config["train"]["adversary"]["step_size"],
                                               max_iter=self.config["train"]["adversary"]["num_steps"],
                                               num_random_init=self.config["train"]["adversary"]["num_random_init"],
                                               batch_size=self.config["train"]["model"]["params"]["batch_size"])
            # Adversarially train model - one step size
            adversarial_trainer = AdversarialTrainer(self.adversarial_model, attacks, ratio=1.0)
            art_data_generator = KerasDataGenerator(
                iterator=((x, y) for (x, y) in self.train_crop_generator.generate(crop_session)),
                size=self.config["train"]["model"]["params"]["batch_size"],
                batch_size=self.config["train"]["model"]["params"]["batch_size"]
            )
            now = datetime.datetime.now()
            experiment_id = now.strftime("%Y-%m-%d_%H:%M:%S")
            for e in range(epoch, epochs + 1):
                print('\nAdversarial training epoch {}/{}'.format(e, epochs))
                for _ in tqdm(range(steps - 1)):
                    adversarial_trainer.fit_generator(
                        generator=art_data_generator,
                        nb_epochs=1,
                        callbacks=extra_callbacks,
                        verbose=0
                    )
                adversarial_trainer.fit_generator(
                    generator=art_data_generator,
                    nb_epochs=1,
                    callbacks=adversarial_model_callbacks(self, e, experiment_id) + extra_callbacks,
                    validation_data=(x_validation, y_validation),
                    verbose=1
                )
        # Stop threads and close sessions
        close(self, crop_session)
        # Return the feature model and validation data
        return self.feature_model, x_validation, y_validation


def check_feature_model(dpmodel):
    if "feature_model" not in vars(dpmodel) or not isinstance(dpmodel.feature_model, keras.Model):
        raise ValueError("Feature model is not properly defined.")


def setup_comet_ml(dpmodel):
    if dpmodel.config["train"]["comet_ml"]["track"]:
        experiment = comet_ml.Experiment(
            api_key=dpmodel.config["train"]["comet_ml"]["api_key"],
            project_name=dpmodel.config["train"]["comet_ml"]["project_name"]
        )
        if "experiment_name" in dpmodel.config["train"]["comet_ml"].keys():
            experiment.set_name(dpmodel.config["train"]["comet_ml"]["experiment_name"])
    else:
        experiment = None
    return experiment


def start_crop_session(dpmodel, configuration):
    crop_graph = tf.Graph()
    with crop_graph.as_default():
        #         cpu_config = tf.ConfigProto(device_count={"CPU": 1, "GPU": 0})
        #         cpu_config.gpu_options.visible_device_list = ""
        crop_session = tf.Session(config=configuration)
        dpmodel.train_crop_generator.start(crop_session)
    gc.collect()
    return crop_session


def tf_configure(dpmodel):
    configuration = tf.ConfigProto()
    # configuration.gpu_options.visible_device_list = dpmodel.config["train"]["gpus"]
    configuration.gpu_options.allow_growth = True
    return configuration


def start_val_session(dpmodel, configuration):
    crop_graph = tf.Graph()
    with crop_graph.as_default():
        val_session = tf.Session(config=configuration)
        keras.backend.set_session(val_session)
        dpmodel.val_crop_generator.start(val_session)
        x_validation, y_validation = deepprofiler.learning.validation.validate(
            dpmodel.config,
            dpmodel.dset,
            dpmodel.val_crop_generator,
            val_session)
    gc.collect()
    return val_session, x_validation, y_validation


def start_main_session(configuration):
    main_session = tf.Session(config=configuration)
    keras.backend.set_session(main_session)
    return main_session


def load_weights(dpmodel, epoch):
    output_file = dpmodel.config["paths"]["checkpoints"] + "/checkpoint_{epoch:04d}.hdf5"
    previous_model = output_file.format(epoch=epoch - 1)
    if epoch >= 1 and os.path.isfile(previous_model):
        if dpmodel.config["train"]["multi_gpu"]:
            dpmodel.parallel_model.layers[-2].load_weights(previous_model)
        else:
            dpmodel.feature_model.load_weights(previous_model)
        print("Weights from previous model loaded:", previous_model)
    else:
        # Initialize all tf variables to avoid tf bug
        keras.backend.get_session().run(tf.global_variables_initializer())


def load_pretrained_weights(dpmodel):
    weight_file = dpmodel.config["paths"]["checkpoints"] + dpmodel.config["profile"]["checkpoint"]
    if os.path.isfile(weight_file):
        if dpmodel.config["train"]["multi_gpu"]:
            dpmodel.parallel_model.layers[-2].load_weights(weight_file)
        elif dpmodel.config["train"]["adversarial"]:
            dpmodel.adversarial_model._model.load_weights(weight_file)
        else:
            dpmodel.feature_model.load_weights(weight_file)
        print("Pre-trained weights loaded from", weight_file)
    else:
        # Initialize all tf variables to avoid tf bug
        keras.backend.get_session().run(tf.global_variables_initializer())


def setup_callbacks(dpmodel):
    output_file = dpmodel.config["paths"]["checkpoints"] + "/checkpoint_{epoch:04d}.hdf5"

    class MultiGPUCheckpoint(keras.callbacks.ModelCheckpoint):

        def set_model(self, model):
            if isinstance(model.layers[-2], keras.models.Model):
                self.model = model.layers[-2]
            else:
                self.model = model

    if dpmodel.config["train"]["multi_gpu"]:
        callback_model_checkpoint = MultiGPUCheckpoint(
            filepath=output_file,
            save_weights_only=True,
            save_best_only=False
        )
    else:
        callback_model_checkpoint = keras.callbacks.ModelCheckpoint(
            filepath=output_file,
            save_weights_only=True,
            save_best_only=False
        )
    csv_output = dpmodel.config["paths"]["logs"] + "/log.csv"
    callback_csv = keras.callbacks.CSVLogger(filename=csv_output)
    callbacks = [callback_model_checkpoint, callback_csv]
    return callbacks


def setup_params(dpmodel, experiment):
    epochs = dpmodel.config["train"]["model"]["epochs"]
    steps = dpmodel.config["train"]["model"]["steps"]
    if dpmodel.config["train"]["comet_ml"]["track"]:
        params = dpmodel.config["train"]["model"]["params"]
        experiment.log_multiple_params(params)
    return epochs, steps


def close(dpmodel, crop_session):
    print("Complete! Closing session.", end=" ", flush=True)
    dpmodel.train_crop_generator.stop(crop_session)
    crop_session.close()
    print("All set.")
    gc.collect()


def adversarial_model_callbacks(dpmodel, epoch_number, experiment_id):
    output_file = dpmodel.config["paths"]["checkpoints"] + "/adv_checkpoint_{epoch:04d}.hdf5"
    callback_model_checkpoint = keras.callbacks.ModelCheckpoint(
        filepath=output_file.format(epoch=epoch_number),
        save_weights_only=True,
        save_best_only=False
    )

    # add real epoch number as a log value
    class EpochNumberLog(Callback):
        def on_epoch_end(self, epoch, logs):
            logs['epoch_number'] = epoch_number
            logs['finish_time'] = datetime.datetime.now()

    csv_output = dpmodel.config["paths"]["logs"] + "/log_" + str(experiment_id) + ".csv"
    callback_csv = keras.callbacks.CSVLogger(filename=csv_output, append=True)
    callbacks = [callback_model_checkpoint, EpochNumberLog(), callback_csv]
    return callbacks


def adversarial_load_weights(dpmodel, epoch):
    output_file = dpmodel.config["paths"]["checkpoints"] + "/adv_checkpoint_{epoch:04d}.hdf5"
    previous_model = output_file.format(epoch=epoch - 1)
    if epoch >= 1 and os.path.isfile(previous_model):
        dpmodel.adversarial_model._model.load_weights(previous_model)
        print("Weights from previous model loaded:", previous_model)
    else:
        # Initialize all tf variables to avoid tf bug
        keras.backend.get_session().run(tf.global_variables_initializer())


def setup_clr_callback(dpmodel):
    return CyclicLR(mode=dpmodel.config["train"]["clr"]["mode"],
                    gamma=dpmodel.config["train"]["clr"]["gamma"],
                    base_lr=dpmodel.config["train"]["clr"]["base_lr"],
                    max_lr=dpmodel.config["train"]["clr"]["max_lr"],
                    step_size=dpmodel.config["train"]["clr"]["step_size"]
                    )
