import importlib

import numpy as np
import pandas as pd

import tensorflow as tf
from keras import backend as K

from art.attacks import ProjectedGradientDescent
from art.classifiers import KerasClassifier

import csv


class Validation(object):

    def __init__(self, config, dset, crop_generator, session):
        self.config = config
        self.dset = dset
        self.crop_generator = crop_generator
        self.session = session
        self.batch_inputs = []
        self.batch_outputs = []

    def process_batches(self, key, image_array, meta):
        # Prepare image for cropping
        total_crops = self.crop_generator.prepare_image(
            self.session,
            image_array,
            meta,
            True
        )
        if total_crops > 0:
            # We expect all crops in a single batch
            batches = [b for b in self.crop_generator.generate(self.session)]
            self.batch_inputs.append(batches[0][0])
            self.batch_outputs.append(batches[0][1])


class PGDValidate(object):

    def __init__(self, config, dset):
        self.config = config
        self.sess = None
        self.dset = dset
        self.x_validation = []
        self.y_validation = []
        self.adversarial_model = None
        self.metrics = ["accuracy"]
        self.crop_generator = importlib.import_module(
            "plugins.crop_generators.{}".format(config["train"]["model"]["crop_generator"])) \
            .GeneratorClass
        self.val_crop_generator = importlib.import_module(
            "plugins.crop_generators.{}".format(config["train"]["model"]["crop_generator"])) \
            .SingleImageGeneratorClass
        self.dpmodel = importlib.import_module("plugins.models.{}".format(config["train"]["model"]["name"])) \
            .ModelClass(config, dset, self.crop_generator, self.val_crop_generator)
        self.val_crop_generator = self.val_crop_generator(config, dset)
        self.dpmodel.feature_model.compile(self.dpmodel.optimizer, self.dpmodel.loss, self.metrics)

    def configure(self):
        # Main session configuration
        configuration = tf.ConfigProto()
        configuration.gpu_options.visible_device_list = self.config["profile"]["gpus"]
        configuration.gpu_options.allow_growth = True
        self.sess = tf.Session(config=configuration)
        self.val_crop_generator.start(self.sess)
        self.x_validation, self.y_validation = validate(
            self.config,
            self.dset,
            self.val_crop_generator,
            self.sess)
        K.set_session(self.sess)

        # Load model
        if self.config["train"]["pretrained"]:
            checkpoint = self.config["paths"]["pretrained"] + "/" + self.config["profile"]["checkpoint"]
        else:
            checkpoint = self.config["paths"]["checkpoints"] + "/" + self.config["profile"]["checkpoint"]
        self.dpmodel.feature_model.load_weights(checkpoint)
        self.adversarial_model = KerasClassifier(model=self.dpmodel.feature_model)

        if "metrics" in self.config["train"]["model"].keys():
            if type(self.config["train"]["model"]["metrics"]) not in [list, dict]:
                raise ValueError("Metrics should be a list or dictionary.")
            keras_metrics = [
                "accuracy",
                "binary_accuracy",
                "categorical_accuracy",
                "sparse_categorical_accuracy",
                "top_k_categorical_accuracy",
                "sparse_top_k_categorical_accuracy"
            ]
            if type(self.config["train"]["model"]["metrics"] is list):
                self.metrics = list(map(lambda metric: importlib.import_module(
                    "plugins.metrics.{}".format(metric)).MetricClass(self.config,
                                                                     metric).f if metric not in keras_metrics else metric,
                                        self.config["train"]["model"]["metrics"]))
            elif type(self.config["train"]["model"]["metrics"] is dict):
                self.metrics = {k: lambda metric: importlib.import_module(
                    "plugins.metrics.{}".format(metric)).MetricClass(self.config,
                                                                     metric).f if metric not in keras_metrics else metric
                                for k, v in self.config["train"]["model"]["metrics"].items()}
        self.dpmodel.feature_model.summary()

    def pgd_validate(self, eps):
        val_x = self.x_validation
        if eps > 0:
            attack = ProjectedGradientDescent(self.adversarial_model,
                                              eps=eps,
                                              eps_step=(eps/self.config["train"]["adversary"]["num_steps"])*2.5,
                                              max_iter=self.config["train"]["adversary"]["num_steps"],
                                              num_random_init=self.config["train"]["adversary"]["num_random_init"])
            val_x = attack.generate(self.x_validation)
        val_loss, val_acc = self.dpmodel.feature_model.evaluate(val_x, self.y_validation,
                                                                batch_size=self.config["train"]["validation"]["batch_size"])
        print("PGD Attack Eps: {}\tValidation Loss:\t{:.4f}\tValidation Accuracy:\t{:.4f}"
              .format(eps, val_loss, val_acc))
        return val_loss, val_acc


def validate(config, dset, crop_generator, session):
    validation = Validation(config, dset, crop_generator, session)
    dset.scan(validation.process_batches, frame="val")
    return np.concatenate(validation.batch_inputs), np.concatenate(validation.batch_outputs)


def pgd_validate(config, dset, max_eps, bin_size, min_eps):
    validation = PGDValidate(config, dset)
    validation.configure()
    eps = min_eps

    columns = ['eps', 'val_loss', 'val_acc', 'model', 'params']
    results_file = config["paths"]["logs"] + "/attack_log.csv"
    with open(results_file, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(columns)
    while eps < max_eps:
        val_loss, val_acc = validation.pgd_validate(eps)
        with open(results_file, 'a') as f:
            writer = csv.writer(f)
            writer.writerow([eps, val_loss, val_acc, config["train"]["model"]["name"], config["train"]["model"]["params"]])
        eps += bin_size
